<?php
   // Обработка запроса регистрации аккаунта
   session_start();
   require_once 'connect.php';

   $login = htmlentities(mysqli_real_escape_string($connect, $_POST['login']));
   $email = htmlentities(mysqli_real_escape_string($connect, $_POST['email']));
   // Хэшируем пароль(в базе нужно просто сохранять хэш и соль)
   $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
   $password_confirm = password_hash($_POST['password_confirm'], PASSWORD_DEFAULT);

   if(password_verify($_POST['password_confirm'], $password)) {
      // загрузка изображения профиля
      // $_FILES['avatar']['name'];
      // time() добавляется для уникальности названия
      
      // $path = 'uploads/' . time() . $_FILES['avatar']['name'];
      // if(!move_uploaded_file($_FILES['avatar']['tmp_name'], '../' . $path)) {
      //    $_SESSION['message'] = 'Ошибка при загрузке изображения';
      //    header('location: ../register.php');
      // }
      
      mysqli_query($connect, "INSERT INTO `users_three` (`id`, `login`, `email`, `password`, `avatar`) VALUES (NULL, '$login', '$email', '$password', '$path')")
         or die("Ошибка " . mysqli_error($connect));
      $_SESSION['message'] = 'Регистрация прошла успешно!';
      header('location: ../signin.php');

      // Проверка прохождении капчи
      // if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {
      //    $secret = '6LcWlI8aAAAAALf7yp4_HVvBj3z_3sx2VydPTwhd';
      //    $ip = $_SERVER['REMOTE_ADDR'];
      //    $response = $_POST['g-recaptcha-response'];
      //    $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$ip");
      //    $arr = json_decode($rsp, TRUE);
      //    if ($arr['success']) {
      //       $_SESSION['message'] = 'Регистрация прошла успешно!';
      //       header('location: ../signin.php');
      //    } else {
      //       $_SESSION['message'] = 'Код капчи не прошёл проверку на сервере!';
      //       header('location: ../register.php');
      //    }
      // }

   } else {
      $_SESSION['message'] = 'Пароли не совпадают';
      header('location: ../register.php');
   }
   
?>

<pre>
   <?php
      // print_r($_POST);
      // print_r($_FILES);
      // echo $path;
      // echo $password;
   ?> 
</pre>