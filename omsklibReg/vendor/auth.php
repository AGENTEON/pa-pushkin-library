<?php
   // обработка запроса аутентификации(логина)
   session_start();
   require_once 'connect.php';

   $login = htmlentities(mysqli_real_escape_string($connect, $_POST['login']));
   $password = $_POST['password'];

   $check_user = mysqli_query($connect, "SELECT `login`, `password` FROM `users_three` WHERE `login` = '$login'")
      or die("Ошибка " . mysqli_error($connect));
   $full_check_user = mysqli_query($connect, "SELECT * FROM `users_three` WHERE `login` = '$login'")
      or die("Ошибка " . mysqli_error($connect));

   // получаем строчку с хэшем пароля из первого запроса к базе(сам пароль в $row[1])
   $row = mysqli_fetch_row($check_user);

   if(!(mysqli_num_rows($check_user) > 0)) {
      $_SESSION['message'] = 'Неправильно введён логин или пароль';
      header('Location: ../signin.php');
   } else {
      if(!password_verify($password, $row[1])) {
         $_SESSION['message'] = 'Неправильный пароль';
         header('Location: ../signin.php');
      } else {
         $user = mysqli_fetch_assoc($check_user);
         $full_user_info = mysqli_fetch_assoc($full_check_user);
   
         $_SESSION['user_info'] = [
            "id" => $full_user_info['id'],
            "login" => $full_user_info['login'],
            "email" => $full_user_info['email'],
            // "avatar" => $full_user_info['avatar'] 
         ];

         setcookie("user_id", $full_user_info['id'], time() + 2600000);
         setcookie("user_login", $full_user_info['login'], time() + 2600000);
         setcookie("user_email", $full_user_info['email'], time() + 2600000);
         
         mysqli_free_result($check_user);
         mysqli_free_result($full_check_user);
         mysqli_close($connect);
         header('Location: ../profile.php');
      }

   }

?>

<pre>
   <?php
      // print_r($check_user);
      // print_r($user);
      // print_r($full_check_user);
      // print_r($full_user_info);
   ?>
</pre>