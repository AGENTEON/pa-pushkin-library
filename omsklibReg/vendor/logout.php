<?php
   // обработка запроса выхода из личного кабинета(выход из аккаунта)

   session_start();
   unset($_SESSION['user_info']);
   setcookie("user_id", "", time() - 2600000);
   header('Location: ../signin.php');
?>