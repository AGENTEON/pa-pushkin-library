$(document).ready(function() {
   // =======================FOR 1st version=========================
   $('#checking-email').keyup(function(e) {

      const email = $('#checking-email').val();
      const submitButton = $('#submit-button');
      //alert(email);
      $.ajax({
         type: 'POST',
         url: './vendor/emailCheckUnique.php',
         data: {
            'check_submit_button': 1,
            'email_id': email,
         },
         success: function(response) { // get the response
            if(response == 1) {
               $('.error-email').css({'display' : 'block', 'backgroundColor' : '#fff', 'color':'#fc037b', 'margin':'5px 5px 0 5px ', 'padding': '5px 5px 0 5px'});
               $('.error-email').text("Данный email уже зарегистрирован.");
               submitButton.attr('disabled', true);
               submitButton.css({'cursor' : 'not-allowed'});
               // console.log("Данный email уже зарегистрирован.");
            } else {
               $('.error-email').css({'display' : 'none', 'color':'#cacaca', 'margin':'5px 5px 0 5px ', 'padding': '5px 5px 0 5px'});
               submitButton.attr('disabled', false);
               submitButton.css({'cursor' : 'pointer'});
               // console.log("Всё в порядке.");
            }
         }
      });
   });

   $('#checking-login').keyup(function(e) {

      const login = $('#checking-login').val();
      const submitButton = $('#submit-button');
      $.ajax({
         type: 'POST',
         url: './vendor/loginCheckUnique.php',
         data: {
            'check_submit_button': 1,
            'login_id': login
         },
         success: function(response) { // get the response
            if(response == 1) {
               $('.error-login').css({'display' : 'block', 'backgroundColor' : '#fff', 'color':'#fc037b', 'margin':'5px 5px 0 5px ', 'padding': '5px 5px 0 5px'});
               $('.error-login').text("Данный логин уже используется.");
               // $('#submitButton').attr('disabled', true);
               submitButton.attr('disabled', true);
               submitButton.css({'cursor' : 'not-allowed'});
               console.log("Login уже используется.")
            } else {
               $('.error-login').css({'display' : 'none', 'color':'#cacaca', 'margin':'5px 5px 0 5px ', 'padding': '5px 5px 0 5px'});
               submitButton.attr('disabled', false);
               submitButton.css({'cursor' : 'pointer'});
               console.log("Всё в порядке с логином.")
            }
         }
      });
   });

});