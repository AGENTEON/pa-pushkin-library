// profile.php
let iframe = document.querySelector('.tab-content');
let iframeContainer = document.querySelector('.tab-content-container');
let gridItems = document.querySelectorAll('.grid-item');

$('#booking-online').click(function() {
   // window.location.href = './profile-pages/booking-online.php';
   // window.open("./profile-pages/booking-online.php", '_blank'); // в новой вкладке, не записывается в историю

   $('.grid-item').removeClass("active");
   $(this).addClass("active");

   iframeContainer.style.display = "block";
   iframe.style.display = "block";

   iframe.src = './profile-pages/booking-online.html';
});

$('#bind-a-ticket').click(function() {
   $('.grid-item').removeClass("active");
   $(this).addClass("active");

   iframeContainer.style.display = "block";
   iframe.style.display = "block";

   iframe.src = './profile-pages/ticket-bind.html';
});

$('#remote-order').click(function() {
   $('.grid-item').removeClass("active");
   $(this).addClass("active");

   iframeContainer.style.display = "block";
   iframe.style.display = "block";

   iframe.src = './profile-pages/remote-order.html';
});

$('#control-order').click(function() {
   $('.grid-item').removeClass("active");
   $(this).addClass("active");

   iframeContainer.style.display = "block";
   iframe.style.display = "block";

   iframe.src = './profile-pages/control-order.html';
});

$('#available-books').click(function() {
   $('.grid-item').removeClass("active");
   $(this).addClass("active");

   iframeContainer.style.display = "block";
   iframe.style.display = "block";

   iframe.src = './profile-pages/available-books.php';
});

// profile-pages/all.php
$('#button-back').click(() => {
   window.history.back();
});