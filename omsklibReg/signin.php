<?php
   session_start();

   if(isset($_SESSION['user_info'])) {
      header('Location: profile.php');
   }
   if(isset($_COOKIE["user_id"])) {
      header('Location: profile.php');
   }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
   <title>Авторизация</title>
   <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
   ></script>
   <link rel="stylesheet" type="text/css" href="./css/signin.css">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   <!-- Bootstrap JS -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   <!-- Bootstrap font-awesome -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>

   <!-- Форма авторизации -->
   <div class="container h-100">
      <div class="row align-items-center h-100">
         <div class="col"></div>
         <div class="col-md-auto">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title header">Добро пожаловать, Читатель!</h5>
                  <form method="POST" action="./vendor/auth.php" id="signin-form">
                     <div class="form-group">
                        <label for="inlineFormInput">Логин</label>
                        <input type="text" name="login" placeholder="Введите логин" class="form-control" required>
                     </div>
                     <div class="form-group">
                        <label for="inlineFormInput">Пароль</label>
                        <input type="password" name="password" placeholder="Введите пароль" class="form-control" required>
                     </div>
                     
                     <div class="form-group">
                        <button class="btn btn-success" type="submit">Войти</button>
                        <!-- <a class="form-field___help" href="./recovery.php">Забыли пароль?</a> -->
                     </div>

                     <!-- <div class="form-group">
                        <div class="form-check">
                           <input id="remember-check" type="checkbox" name="remember-check" class="form-check-input">
                           <label for="remember-check" class="form-check-label">Запомнить меня</label>
                        </div>
                     </div> -->

                     <div class="form-group">
                        <p class="bottom-text">
                           У вас нет аккаунта? - <a href="./register.php">Зарегистрируйтесь!</a>
                        </p>
                     </div>

                     <?php 
                        if(isset($_SESSION['message'])) {
                           echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
                        }
                        unset($_SESSION['message']);
                     ?>
                  </form>
               </div>
            </div>
         </div>
         <div class="col"></div>
      </div>
   </div>


</body>
</html>