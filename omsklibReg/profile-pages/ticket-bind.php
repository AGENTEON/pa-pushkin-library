<?php
   session_start();

   if(!isset($_SESSION['user_info'])) {
      header('Location: ../signin.php');
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Привязка читательского билета</title>
   <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
   ></script>
   <link rel="stylesheet" type="text/css" href="../css/profile.css">
   <!-- BOOTSTRAP CDN CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   <!-- BOOTSTRAP CDN JS -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   <!-- FONTS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>
   

   <div class="container">
      <!-- Navbar -->
      <nav class="navbar">
         <span class="navbar-text navbar-header-text">
            Привязка читательского билета к аккаунту
         </span>
         <span class="navbar-text">
            <a href="#"><span class="navbar-email-text"><?=$_SESSION['user_info']['email'];?></span></a>
            <a href="./vendor/logout.php" class="logout"><button class="btn btn-danger">Выход</button></a>
         </span>
      </nav>

      <!-- Профиль / личный кабинет -->
      <div class="card">
         <div class="card-body">
            <ul class="nav justify-content-start">
               <li class="nav-item">
                  <a class="active" id="button-back" style="cursor: pointer">Назад</a>
               </li>
               
            </ul>

            <form class="form">
            <div class="form-group row">
               <label for="inputTicketCode" class="col-sm-4 col-form-label">Введите код читательского билета</label>
               <div class="col-sm-8">
                  <input type="text" class="form-control" id="staticEmail" placeholder="13 символов">
               </div>
            </div>

            <button type="submit" class="btn btn-primary mb-2">Привязать билет</button>
            </form>
         </div>
      </div>
   </div>
   

   <script src="../js/scripts/profile.js"></script>
</body>
</html>