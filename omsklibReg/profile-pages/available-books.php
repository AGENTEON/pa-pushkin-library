<?php
   session_start();

   if(!isset($_SESSION['user_info'])) {
      header('Location: ../signin.php');
   }
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Книга на руках</title>
   <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
   ></script>
   <link rel="stylesheet" type="text/css" href="../css/available-books.css">
   <!-- BOOTSTRAP CDN CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   <!-- BOOTSTRAP CDN JS -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   <!-- FONTS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body> 
   <div class="container">
      <p class="header-text">Книги на руках</p>

      <p class='card-text' style="color: #663300; font-size: 18зч; font-weight: bold;">
         На <?=date('m/d/Y', time());?> у вас на руках следующие издания взятые в ОГОНБ имени А.С. Пушкина:
      </p>

      <form class="form">

      <!-- <button type="submit" class="btn btn-primary mb-2">Назад</button> -->
      </form>
   </div>
   

   <script src="../js/scripts/profile.js"></script>
</body>
</html>