<?php
   session_start();

   if(!isset($_SESSION['user_info'])) {
      header('Location: signin.php');
   }
   // if(!isset($_COOKIE['user_id'])) {
   //    header('Location: signin.php');
   // }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>Личный кабинет</title>
   <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
   ></script>
   <link rel="stylesheet" type="text/css" href="css/profile.css">
   <!-- BOOTSTRAP CDN CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   <!-- BOOTSTRAP CDN JS -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   <!-- FONTS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
   <link rel="preconnect" href="https://fonts.gstatic.com">
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap" rel="stylesheet">
</head>
<body>

<div class="container">
   <!-- Navbar -->
   <nav class="navbar">
      <span class="navbar-text navbar-header-text">
         Кабинет читателя
      </span>
      <span class="navbar-text">
         <a href="#"><span class="navbar-email-text"><?=$_SESSION['user_info']['email'];?></span></a>
         <a href="./vendor/logout.php" class="logout"><button class="btn btn-danger">Выход</button></a>
      </span>
   </nav>

   <!-- Профиль / личный кабинет -->
   <div class="card">
      <div class="card-body">
         <div class="grid-tabs-container">

            <div class="grid-item active" id="booking-online">
               <div class="grid-img-wrapper">
                  <img src="./assets/icons/record.png" alt=":/"></img>
               </div> 
               <p class="grid-item-text">Запись</p>
            </div>

            <div class="grid-item" id="bind-a-ticket">
               <div class="grid-img-wrapper">
                  <img src="./assets/icons/confirming.png" alt=":/"></img>
               </div> 
               <p class="grid-item-text">Привязать билет</p>
            </div>

            <div class="grid-item" id="remote-order">
               <div class="grid-img-wrapper">
                  <img src="./assets/icons/order.png" alt=":/"></img>
               </div> 
               <p class="grid-item-text">Заказ</p>
            </div>

            <div class="grid-item" id="control-order">
               <div class="grid-img-wrapper">
                  <img src="./assets/icons/control.png" alt=":/"></img>
               </div> 
               <p class="grid-item-text">Контроль заказов</p>
            </div>

            <div class="grid-item" id="available-books">
               <div class="grid-img-wrapper">
                  <img src="./assets/icons/books.png" alt=":/"></img>
               </div> 
               <p class="grid-item-text">Книги</p>
            </div>
         </div>
         
         <hr class="hr-line">
         
         <div class="tab-content-container"><br>
            <iframe class="tab-content" src="./profile-pages/booking-online.html"> 
               Ваш браузер не поддерживает фреймы!
            </iframe>
         </div>
      </div>
   </div>
</div>

<script src="./js/scripts/profile.js"></script>

</body>
</html>