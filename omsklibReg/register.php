<?php
   session_start();

   if(isset($_SESSION['user_info'])) {
      header('Location: profile.php');
   }

?>

<!DOCTYPE html>
<html lang="ru">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
   <title>Регистрация</title>
   <script
      src="https://code.jquery.com/jquery-3.5.1.min.js"
      integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
      crossorigin="anonymous"
   ></script>
   <!-- reCAPTCHA JS -->
   <script src="https://www.google.com/recaptcha/api.js" async defer></script>
   <!-- Check email AND login existance in db -->
   <script src="./js/emailUniqueCheck.js"></script>
   <!-- CUSTOM JS -->
   <script src="./js/scripts/register.js"></script>
   <!-- <script src="./js/recaptcha-submit-button.js"></script> -->
   <!-- CUSTOM CSS -->
   <link rel="stylesheet" type="text/css" href="css/register.css">
   <!-- BOOTSTRAP CDN CSS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
   <!-- BOOTSTRAP CDN JS -->
   <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
   <!-- FONTS -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<body>

   <!-- Форма регистрации -->
   <div class="container h-100" id="form">
      <div class="row align-items-center h-100">
         <div class="col"></div>
         <div class="col-md-auto">
            <div class="card">
               <div class="card-body">
                  <h5 class="card-title header">Регистрация</h5>
                  <form id="register-form" method="POST" action="vendor/signup.php" enctype="multipart/form-data">
                     <div class="form-group">
                        <!-- Валидация логина -->
                        <label for="inlineFormInput">Логин</label>
                        <input class="form-control" id="checking-login" type="text" name="login" placeholder="Придумайте логин для входа" required minlength="4" maxlength="64">
                        <small class="error-login" style="color: red;"></small>
                     </div>
                     <div class="form-group">
                        <label for="inlineFormInput">Почта</label>
                        <!-- pattern="[-a-zA-Z0-9~!$%^&amp;*_=+}{'?]+(\.[-a-zA-Z0-9~!$%^&amp;*_=+}{'?]+)*@([a-zA-Z0-9_][-a-zA-Z0-9_]*(\.[-a-zA-Z0-9_]+)*\.([cC][oO][mM]))(:[0-9]{1,5})?" -->
                        <!-- pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum)\b" -->
                        <input class="form-control" id="checking-email" type="email" name="email" placeholder="Введите адрес эл. почты" 
                           required minlength="3" maxlength="64" pattern="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+(?:[A-Z]{2}|com|org|net|gov|mil|biz|info|mobi|name|aero|jobs|museum|ru)\b">
                        <small class="error-email" style="color: red;"></small>
                     </div>
                     <div class="form-group">
                        <label for="inlineFormInput">Пароль</label>
                        <input class="form-control" type="password" name="password" placeholder="Введите пароль" required minlength="3" maxlength="64">
                     </div>
                     <div class="form-group">
                        <label for="inlineFormInput">Подтвердите пароль</label>
                        <input class="form-control" type="password" name="password_confirm" placeholder="Повторите пароль" required>
                     </div>
                     <!-- Валидация и вывод сообщения о её результатах -->
                     <?php 
                        if(isset($_SESSION['message'])) {
                           echo '<p class="msg"> ' . $_SESSION['message'] . ' </p>';
                        }
                        unset($_SESSION['message']);
                     ?>

                     <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="checkbox">
                        <div class="reg-info-wrapper">
                           <label id="checkbox-label" class="form-check-label">Нажимая на кнопку «Зарегистрироваться», я принимаю <a target="_blank" href="./agreement/user-agreement.html">Пользовательское соглашение</a> и подтверждаю, что ознакомлен и согласен с <a target="_blank" href="./agreement/privacy-policy.html">Политикой конфиденциальности</a> данного сайта.</label>
                        </div>
                        <br>
                     </div>
                     
                     <div class="form-group">
                        <button id="submit-button" type="submit" name="registerButton" class="btn btn-success lock">Зарегистрироваться</button>
                     </div>

                     <div class="form-group">
                        <p>
                           У вас уже есть аккаунт? - <a href="./signin.php">Авторизируйтесь!</a>
                        </p>
                     </div>

                  </form>
               </div>
            </div>
         </div>
         <div class="col"></div>
      </div>
   </div>

</body>
</html>